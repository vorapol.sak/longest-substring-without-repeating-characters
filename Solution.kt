class Solution {
    fun lengthOfLongestSubstring(s: String): Int {
        var strResult : String = ""
        var index = 0
        val size = s.length
        val sizeIndex = size-1
        var temp = StringBuilder()
        s.forEach { it ->
            temp.append(it)
            for(i in (index+1)..(sizeIndex)){
                if(temp.indexOf(s[i]) == -1){
                    temp.append(s[i])
                    if((i+1) == size) {
                        if(temp.toString().length > strResult.length) {
                            strResult = temp.toString()
                        }
                    }
                } else {
                    if(temp.toString().length > strResult.length) {
                        strResult = temp.toString()
                    }
                    break
                }
            }
            if((index+1) == s.length) {
                if(temp.toString().length > strResult.length) {
                    strResult = temp.toString()
                }
            }
            temp.clear()
            index++
        }
        return strResult.length
    }
}